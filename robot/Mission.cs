using System;
using Finit.Robot;

namespace Finit
{
    public enum MissionOutcome
    {
        OutOfFuel,
        OutOfTime,
        Exploded,
        SavedTheWorld
    }

    public struct MissionResult
    {
        public double Mileage;
        public double FuelInitial;
        public double FuelRemaining;
        public int CodebreakAttempts;
        public int CodebreakAttemptsMax;
        public int TurnsMax;
        public int TurnsRemaining;
        public bool BombDefused;
        public MissionOutcome Outcome;

        override public string ToString()
        {
            return String.Format(
                "Mileage:\t\t{0}\r\n" +
                "Turns remaining:\t{1}/{2}\r\n" +
                "Fuel remaining:\t\t{3}/{4}\r\n" +
                "Codebreak attempts:\t{5}/{6}\r\n" +
                "Bomb defused:\t\t{7:b}\r\n" +
                "Mission outcome:\t{8}\r\n",
                Mileage,
                TurnsRemaining, TurnsMax,
                FuelRemaining, FuelInitial,
                CodebreakAttempts, CodebreakAttemptsMax,
                BombDefused,
                Outcome
            );
        }
    }

    public class Mission
    {
        private static double DEFAULT_INITIAL_FUEL = 1000.0;
        private static int DEFAULT_MAX_TURNS = 10000;

        private IRobot Robot;
        private Room Room;
        private double InitialFuel;
        private int? CodebreakAttempts;
        private int TurnsMax;

        public Mission(IRobot robot, Room room)
        {
            Robot = robot;
            Room = room;
            InitialFuel = DEFAULT_INITIAL_FUEL;
            TurnsMax = DEFAULT_MAX_TURNS;
            CodebreakAttempts = null;
        }

        public Mission(IRobot robot, Room room, int turns, double initialFuel) : this(robot, room)
        {
            TurnsMax = turns;
            InitialFuel = initialFuel;
        }

        public Mission(IRobot robot, Room room, int turns, double initialFuel, int codebreakAttempts) : this(robot, room, turns, initialFuel)
        {
            CodebreakAttempts = codebreakAttempts;
        }

        public MissionResult Run()
        {
            if (CodebreakAttempts.HasValue) {
                Room.CodebreakAttemptsMax = CodebreakAttempts.Value;
            }

            Hardware hardware = new Hardware();
            hardware.AttachSensor("radiation", new Sensor(() => Room.GetRadiationLevel(Room.RobotPosition)));
            hardware.AttachSensor("proximity", new Sensor(() => Room.IsRobotFacingWall() ? 1.0 : 0.0));
            hardware.AttachSensor("illumination", new Sensor(() => Room.GetIlluminationLevel()));
            hardware.AttachSensor("sniffer", new Sensor(() => Room.GetCodeSignal()));
            hardware.codebreaker = new Manipulator<string>((code) => Room.TryBreakCode(code));

            double mileage = 0.0;
            double fuel = InitialFuel;
            int turnsRemaining = TurnsMax;

            MissionResult result = new MissionResult();
            result.TurnsMax = TurnsMax;
            result.FuelInitial = InitialFuel;
            result.CodebreakAttemptsMax = Room.CodebreakAttemptsMax;

            Robot.Reboot();
            while (true) {
                Room.NextTurn();

                try {
                    Robot.DoTurn(hardware);
                } catch {
                    result.Outcome = MissionOutcome.Exploded;
                    break;
                }

                switch (hardware.NextCommand) {
                    case Command.DriveStraight:
                        if (fuel <= 0) {
                            break;
                        }

                        if (Room.AdvanceRobot()) {
                            mileage += 1.0;
                        }
                        fuel -= 1.0;
                        break;
                    case Command.TurnLeft:
                        Room.TurnRobotLeft();
                        break;
                    case Command.TurnRight:
                        Room.TurnRobotRight();
                        break;

                    default:
                        break;
                }

                turnsRemaining -= 1;

                if (Room.Bomb.Defused) {
                    result.BombDefused = true;
                    result.Outcome = MissionOutcome.SavedTheWorld;
                    break;
                }

                if (!Room.HasCodebreakAttempts()) {
                    result.Outcome = MissionOutcome.Exploded;
                    break;
                }

                if (turnsRemaining <= 0) {
                    result.Outcome = MissionOutcome.OutOfTime;
                    break;
                }

                if (fuel <= 0 && !Room.IsBombAt(Room.RobotPosition)) {
                    result.Outcome = MissionOutcome.OutOfFuel;
                    break;
                }
            }

            result.Mileage = mileage;
            result.CodebreakAttempts = Room.CodebreakAttempts;
            result.FuelRemaining = fuel;
            result.TurnsRemaining = turnsRemaining;
            
            return result;
        }
    }
}

