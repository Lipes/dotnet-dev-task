using System;
using System.Collections.Generic;

namespace Finit.Robot
{
    public enum Command {
        Noop,
        DriveStraight,
        TurnLeft,
        TurnRight
    };

    public class Hardware
    {
        public Command NextCommand = Command.Noop;
        public Dictionary<string,Sensor> sensors = new Dictionary<string,Sensor>();
        public Manipulator<string> codebreaker;

        public void AttachSensor(string name, Sensor sensor)
        {
            sensors[name] = sensor;
        }
    }
}

